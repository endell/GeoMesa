package com.geomesa.spark.SparkJTS;

import com.vividsolutions.jts.geom.Geometry;

import java.util.ArrayList;
import java.util.List;

/**
 * union线路合并，并且生成交叉点
 */
public class UnionLine {

    private static GeometryFactory factory = GeometryFactory.getInstance();

    public static void main(String[] args) {
        List<Geometry> list = new ArrayList<Geometry>();
        list.add(factory.buildGeo("LINESTRING (10 10,2 2,0 0)"));
        list.add(factory.buildGeo("LINESTRING (10 0,6 6,0 10)"));
        list.add(factory.buildGeo("LINESTRING (1 1,3 1,10 1)"));
        Geometry nodedLine = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            nodedLine = nodedLine.union(list.get(i));
        }
        int num = nodedLine.getNumGeometries();
        for (int j = 0; j < num; j++) {
            Geometry eachG = nodedLine.getGeometryN(j);
            System.out.println("union线路合并，并且生成交叉点："+eachG.toText());
        }
    }
}
