package com.geomesa.spark.SparkJTS;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.operation.linemerge.LineMerger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * LineMerger 线路合并，线路之间不能有交点，并且只在线路末尾有公共交点
 */
public class MergerLine {

    private static GeometryFactory factory = GeometryFactory.getInstance();


    public static void main(String[] args) {
        LineMerger lineMerger = new LineMerger();
        List<Geometry> list = new ArrayList<Geometry>();
        list.add(factory.buildGeo("LINESTRING (3 3,2 2,0 0)"));
        list.add(factory.buildGeo("LINESTRING (3 3,6 6,0 10)"));
        list.add(factory.buildGeo("LINESTRING (0 10,3 1,10 1)"));
        lineMerger.add(list);
        Collection<Geometry> mergerLineStrings = lineMerger.getMergedLineStrings();
        for(Geometry g : mergerLineStrings){
            System.out.println("线路合并，线路之间不能有交点，并且只在线路末尾有公共交点："+g.toText());
        }
    }
}
