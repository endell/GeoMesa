package com.geomesa.spark.SparkJTS;

import com.vividsolutions.jts.algorithm.CGAlgorithms;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class GeometryUtils {

    private static com.geomesa.spark.SparkJTS.GeometryFactory factory = com.geomesa.spark.SparkJTS.GeometryFactory.getInstance();

    public static List readLine(String fileName) throws IOException {
        InputStreamReader isr = new InputStreamReader(GeometryUtils.class.getResourceAsStream("/" + fileName));
        BufferedReader br = new BufferedReader(isr);
        String ln = null;
        List listLine = new ArrayList();
        JSONArray features = new JSONArray();
        while ((ln = br.readLine()) != null) {
            JSONObject jo = new JSONObject(ln);
            features = jo.getJSONArray("features");
        }
        //System.out.println("features："+features.length());
        for (int i = 0; i < features.length(); i++) {
            JSONObject info = features.getJSONObject(i);
            JSONObject geometry = info.getJSONObject("geometry");
            List list = geometry.getJSONArray("coordinates").toList();
            System.out.println(list.get(0));
            listLine.add(list.get(0).toString().replace("[", "").replace("]", ""));
            //System.out.println(listLine.get(i));
        }
        Coordinate[] coordinates = new Coordinate[99];
        System.out.println(listLine.size());
        for (int i = 0; i < listLine.size(); i++) {
            //System.out.println(listLine.get(i));
            String[] strings = listLine.get(i).toString().split(",");
            coordinates[i] = new Coordinate(Double.parseDouble(strings[0]), Double.parseDouble(strings[1]));
            //1.357846016964474E7,4505819.85796843
            System.out.println("距离：" + CGAlgorithms.distancePointLine(new Coordinate(1.357846016964474E7, 4505819.85796843), new Coordinate[]{coordinates[i]}));
            //System.out.println("点是否在线上："+CGAlgorithms.isOnLine(new Coordinate(1.357846016964474E7, 4505819.85796843),new Coordinate[]{coordinates[i]}));

        }

        GeometryFactory geometryFactory = new GeometryFactory();
        Point p1 = geometryFactory.createPoint(new Coordinate(1.357846016964474E7, 4505819.85796843));
        Point p2 = geometryFactory.createPoint(new Coordinate(1.35784487570592E7, 4505814.57812586));
        double d = p1.distance(p2);
        System.out.println("两点的距离：" + d);

        Operation operation = new Operation();
        System.out.println("测试距离：" + operation.distanceGeo(p1, p2));

        return null;
    }

    public static void main(String[] args) throws IOException {
        readLine("linepp.txt");
    }
}
