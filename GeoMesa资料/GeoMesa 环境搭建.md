﻿@[toc](GeoMesa 环境搭建)

# 版本

以centos7为操作系统，搭建hadoop、hbase单机环境，使用geomesa-hbase导入数据，使用geoserver展示数据的过程，其间夹杂着其它环境部署的事项。

## 虚拟机安装
windows10下安装vmpro14，bios下开启hyper虚拟化功能，安装，填写序列号。
 - [vm VMware-workstation-full  虚拟机安装](http://note.youdao.com/noteshare?id=fe6daf1e78c860f02b9c1d898a2d3d02&sub=C9640F0038344F3186D608539AB3928D)

## os centos7    Centos安装
下载centos7，在vm中安装，设置nat网关、static ip、hostname、hosts、永久关闭防火墙，克隆机器，修改机器名称，网卡及ip地址，在hosts中配置主机名和ip地址映射
 - [os centos7    Centos安装](http://note.youdao.com/noteshare?id=ed026e6822113e6b12a9d95fbfb1aeae&sub=3D1A8B11A61F417E9C3D2C4CB6AA374B)

## CentOS安装Jdk并配置环境变量
 - [jdk   JDK安装](https://www.cnblogs.com/panzhaohui/p/6306675.html)

## hadoop、hbase环境部署
 - hadoop 2.6.5

> 单机部署：
大数据学习系列之一 ----- Hadoop环境搭建(单机)
https://www.cnblogs.com/xuwujing/p/8017108.html
大数据学习系列之二 ----- HBase环境搭建(单机)
https://www.cnblogs.com/xuwujing/p/8017116.html
> 
> 集群部署： CentOS7搭建 Hadoop + HBase + Zookeeper集群
> https://my.oschina.net/kolbe/blog/908526
> 
> CentOS7搭建 Hadoop + HBase + Zookeeper集群
> https://blog.csdn.net/zuochao_2013/article/details/72726378
> 
> Java API 与Hbase集群交互常用操作总结
> https://blog.csdn.net/z69183787/article/details/52895121


## geomesa_hbase部署
使用命令行进行数据导入，hadoop、hbase部署好后，可以使用supermap iserver进行数据的导入、发布、地图服务浏览展示等操作，这里介绍geomesa_hbase的部署
[GeoMesa-HBase部署实践](https://blog.csdn.net/xiaof22a/article/details/80215787)

## geoserver 2.14.2

[geoserver部署：](http://note.youdao.com/noteshare?id=779c5e66a227caf029715946791885c2&sub=ECA3A4595EA24ED1B9CAAAFBC2C7FA92)

[centos7安装geomesa2.0.2_hbase_geoserver2.13.2的方法](https://blog.csdn.net/hsg77/article/details/82221531)


> 山华水清 : [3.0.geomesa使用环境搭建过程](https://www.jianshu.com/p/93071dd0074d)

